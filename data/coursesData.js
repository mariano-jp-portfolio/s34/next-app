export default [
	{
		id: "CFF-001",
		name: "HTML & CSS",
		description: "Foundational Skills of a Web Dev",
		price: 20000,
		onOffer: true,
		start_date: "Feb 20, 2021",
		end_date: "August 19, 2021"
	},
	{
		id: "CFF-002",
		name: "Vanilla JavaScript",
		description: "Good ol' JavaScript",
		price: 45000,
		onOffer: true,
		start_date: "March 15, 2021",
		end_date: "May 12, 2021"
	},
	{
		id: "CFF-003",
		name: "JavaScript Frameworks",
		description: "Next-gen JS functionality",
		price: 123000,
		onOffer: true,
		start_date: "April 16, 2021",
		end_date: "September 18, 2021"
	}
];