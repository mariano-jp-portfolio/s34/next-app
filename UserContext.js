// Base import
import React from 'react';

// create a context object
const UserContext = React.createContext();

// Export UserProvider
// Every context object comes with a Provider React component that allows consuming components to subscribe to context changes
export const UserProvider = UserContext.Provider;

export default UserContext;