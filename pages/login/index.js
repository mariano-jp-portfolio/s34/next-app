// Base import
import React, { useContext, useState, useEffect } from 'react';

// React-Bootstrap
import { Container, Form, Button } from 'react-bootstrap';

// Users Data
import usersData from '../../data/usersData';

// UserContext
import UserContext from '../../UserContext';

// Router
import Router from 'next/router';

// Export Login functional component
export default function Login() {
	// Destructuring UserContext
	const { user, setUser } = useContext(UserContext);
	
	// States
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(false);
	
	// To determine if the button will be disabled or not
	useEffect(() => {
		if (email !== '' && password !== '') {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email, password]);
	
	// Authenticating user details
	function authenticate(e) {
		e.preventDefault();
		
		// Authenticate based on usersData while comparing it to the user's input
		const match = usersData.find((user) => {
			return (user.email === email && user.password === password);
		});
		
		if (match) {
			// temporarily save the details in the user's local storage
			localStorage.setItem('email', email);
			localStorage.setItem('isAdmin', match.isAdmin);
			
			// set the captured data to be the global scope for the user state
			setUser({
				email: localStorage.getItem('email'),
				isAdmin: match.isAdmin
			});
			
			// simple greeting to the user
			alert(`Hi ${email}, welcome back!`);
			
			// redirect to courses page
			Router.push('/courses');
		} else {
			alert('No such user found.');
		}
		
		// Set the input fields to be blank after submitting the form
		setEmail('');
		setPassword('');
		
		// // simple greeting to the user
		// alert(`Hi ${email}, welcome back!`);
	}
	
	// Form
	return (
		<React.Fragment>
			<Container className="my-3" fluid>
				<h2>Login</h2>
					<Form onSubmit={authenticate}>
						<Form.Group controlId="formBasicEmail">
							<Form.Label><h5>Email</h5></Form.Label>
							<Form.Control 
								type="email"
								value={email}
								placeholder="Your email"
								onChange={(e) => setEmail(e.target.value)}
								required
							/>
						</Form.Group>
						
						<Form.Group controlId="formBasicPassword">
							<Form.Label><h5>Password</h5></Form.Label>
							<Form.Control 
								type="password"
								value={password}
								placeholder="Your password"
								onChange={(e) => setPassword(e.target.value)}
								required
							/>
						</Form.Group>
						{isActive ?
							<Button className="btn btn-block" variant="primary" type="submit">Login</Button>
							:
							<Button className="btn btn-block" variant="secondary" disabled>Login</Button>
						}
					</Form>
				</Container>
		</React.Fragment>
	);
};