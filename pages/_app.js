// Base import
import { Fragment, useState, useEffect } from 'react';

// React-Bootstrap
import 'bootstrap/dist/css/bootstrap.min.css';
import { Container } from 'react-bootstrap';

// Global CSS
import '../styles/globals.css';

// Context Provider
import { UserProvider } from '../UserContext';

// Navbar import
import NavBar from '../components/Navbar';

export default function MyApp({ Component, pageProps }) {
  // State hook for user state for global scope
  const [user, setUser] = useState({
  		email: null,
  		isAdmin: null
  });
  
  // localStorage can only be accessed after this component has been rendered
  useEffect(() => {
  		setUser({
  			email: localStorage.getItem('email'),
  			isAdmin: localStorage.getItem('isAdmin') === 'true'
  		});
  }, []);
  
  // Effect hook for testing setUser() functionality
  useEffect(() => {
  		console.log(user.email);
  }, [user.email]);
  
  // Function for clearing localStorage upon log out
  const unsetUser = () => {
  		localStorage.clear();
  		
  		// set the user global scope in the context provider to have its email and isAdmin set to null
  		setUser({
  			email: null,
  			isAdmin: null
  		});
  };
  
  
  return (
  		<Fragment>
  			<UserProvider value={{user, setUser, unsetUser}}>
  				<NavBar />
  				<Container fluid>
  					<Component {...pageProps} />
  				</Container>
  			</UserProvider>
  		</Fragment>
  	);
};