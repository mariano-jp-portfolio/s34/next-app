// Export Contact page functional component
export default function Contact() {
	return (
		<div>
			<h1>Contact Us</h1>
			<h4>Mobile No:</h4>
			<p>0956-120-7959</p>
			<h4>Address:</h4>
			<p>123 Main St., San Francisco, California 94102</p>
		</div>
	);
};