// Base import
import React, { useContext } from 'react';
import Head from 'next/head';

// UserContext
import UserContext from '../../UserContext';

// Course Data
import coursesData from '../../data/coursesData';

// CourseCard component
import CourseCard from '../../components/CourseCard';

// React-Bootstrap
import { Table, Button } from 'react-bootstrap';

// Export Course functional component
export default function Course() {
	const { user } = useContext(UserContext);
	
	// View for regular users
	const courses = coursesData.map((indivCourse) => {
		return (
			<CourseCard 
				key={indivCourse.id}
				courseProp={indivCourse}
			/>
		);
	});
	
	// View for Admin
	const courseRow = coursesData.map((indivCourse) => {
		return (
			<tr key={indivCourse.id}>
				<td>{indivCourse.id}</td>
				<td>{indivCourse.name}</td>
				<td>{indivCourse.description}</td>
				<td>{indivCourse.price}</td>
				<td>{indivCourse.onOffer ? 'Open' : 'Close'}</td>
				<td>{indivCourse.start_date}</td>
				<td>{indivCourse.end_date}</td>
				<td>
					<Button variant="success">Update</Button>
					<Button variant="danger">Disable</Button>
				</td>	
			</tr>
		);
	})
	
	// Conditional rendering for if a user is an admin or not
	return (
		user.isAdmin === true
		?
		<React.Fragment>
			<Head>
				<title>Admin's Course Dashboard</title>
			</Head>
			<h1>Course Dashboard</h1>
			<Table striped bordered hover>
				<thead>
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th>Description</th>
						<th>Price</th>
						<th>Availability</th>
						<th>Start Date</th>
						<th>End Date</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					{ courseRow }
				</tbody>
			</Table>
		</React.Fragment>
		:
		<React.Fragment>
			{ courses }
		</React.Fragment>
	);
};