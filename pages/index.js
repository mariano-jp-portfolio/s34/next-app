// Base import
import Head from 'next/head';

// Home CSS
// import styles from '../styles/Home.module.css';

// React-Bootstrap
import { Container } from 'react-bootstrap';

// Component import
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

// Export Home page functional component
export default function Home() {
  const data = {
  		title: "Code for Fun!",
  		content: "It's not a bug, it's an undocumented feature",
  		destination: "/course",
  		label: "View Our Courses"
  }
  return (
    <Container fluid>
      <Banner dataProp={data} />
      <Highlights />
    </Container>
  );
};