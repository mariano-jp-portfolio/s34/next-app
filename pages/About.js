// Export About page functional component
export default function About() {
	return (
		<div>
			<h1>About Us</h1>
			<p>
				Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ullam quisquam minima odio laudantium maxime exercitationem, itaque perferendis totam quod accusamus repellendus dolor necessitatibus, culpa iusto architecto aliquid, nesciunt sint ut.
			</p>
			<p>
				Lorem, ipsum dolor sit amet, consectetur adipisicing elit. Doloremque accusantium, id, consequatur mollitia dicta atque est nobis magni sunt repellat quia impedit distinctio omnis quis beatae cum totam, earum molestias!
			</p>
		</div>
	);
};