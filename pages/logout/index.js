// Base import
import { useContext, useEffect } from 'react';

// UserContext
import UserContext from '../../UserContext';

// Router
import Router from 'next/router';

// Export Logout functional component
export default function index() {
	const {unsetUser, setUser} = useContext(UserContext);
	
	useEffect(() => {
		unsetUser();
		
		// to redirect back to login
		Router.push('/login');
	}, []);
	
	return null;
};