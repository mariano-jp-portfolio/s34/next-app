// Link
import Link from 'next/link';

// PropTypes
import PropTypes from 'prop-types';

// React-Bootstrap
import { Row, Col, Jumbotron } from 'react-bootstrap';

// Export Banner functional component
export default function Banner({dataProp}) {
	// destructure the data prop into its properties
	const { title, content, destination, label } = dataProp;
	
	return (
		<Row>
			<Col>
				<Jumbotron className="my-2">
					<h1>{title}</h1>
					<h5>
						{content}
					</h5>
					<p>
						<a as={Link} href={destination}>{label}</a>
					</p>
				</Jumbotron>
			</Col>
		</Row>
	);
};

// Checks if the Banner component is getting the correct data types
Banner.propTypes = {
	// shape() is used to check if the prop object conforms to a specific data type
	data: PropTypes.shape({
		// define the props and their expected types
		title: PropTypes.string.isRequired,
		content: PropTypes.string.isRequired,
		destination: PropTypes.string.isRequired,
		label: PropTypes.string.isRequired
	})
};