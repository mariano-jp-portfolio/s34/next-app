// React-Bootstrap
import { Row, Col, Card } from 'react-bootstrap';

// Export Highlights functional component
export default function Highlights() {
	return (
		<Row className="my-3">
			<Col>
				<Card className="cardHighlights">
					<Card.Body className="cardBG">
						<Card.Title>
							<h2>Learn from Home</h2>
						</Card.Title>
						<Card.Text>
							<p>
									Lorem ipsum dolor sit amet consectetur adipisicing elit. Sapiente, totam laudantium quos pariatur temporibus corrupti in minima quod dolorum laboriosam esse, ex, voluptatibus assumenda voluptatem modi doloremque consequatur iure. Nobis?
							</p>
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			
			<Col>
				<Card className="cardHighlights">
					<Card.Body className="cardBG">
						<Card.Title>
							<h2>Study Now, Pay Later</h2>
						</Card.Title>
						<Card.Text>
							<p>
								Lorem ipsum dolor, sit amet consectetur adipisicing elit. Sint saepe at ratione culpa, eaque blanditiis reiciendis ipsam quibusdam consequatur. Maiores deserunt saepe officiis, inventore optio commodi labore, reprehenderit placeat voluptas.
							</p>
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
				
			<Col>
				<Card className="cardHighlights">
					<Card.Body className="cardBG">
						<Card.Title>
							<h2>Be a Part of Our Family</h2>
						</Card.Title>
						<Card.Text>
							<p>
								Lorem ipsum, dolor, sit amet consectetur adipisicing elit. Enim, magni quidem commodi ipsum et rem deleniti sint, voluptatem assumenda fugit molestiae animi laboriosam nemo itaque accusamus dolorum, sit modi vel!
							</p>
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
		</Row>
	);
};