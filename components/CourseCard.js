// Base import
import { useState, useEffect } from 'react';

// React-Bootstrap
import { Container, Card, Button } from 'react-bootstrap';

// PropTypes
import PropTypes from 'prop-types';

// Export CourseCard functional component
export default function CourseCard({courseProp}) {
	// destructure course
	const { name, description, price, start_date, end_date} = courseProp;
	
	const [count, setCount] = useState(0);
	const [seats, setSeats] = useState(10);
	const [isOpen, setIsOpen] = useState(true);
	
	function enroll() {
		setCount(count + 1);
		setSeats(seats - 1);
	}
	
	useEffect(() => {
		if (seats === 0) {
			setIsOpen(false);
		}
	}, [seats])
	
	
	return (
		<Container className="my-3" fluid>
			<Card>
				<Card.Body className="cardBG">
					<Card.Title><h3>{name}</h3></Card.Title>
					<Card.Text>
						<p className="subtitle">{description}</p>
						<p className="subtitle">&#8369;{price}</p>
						<p>Start Date: {start_date} <br /><span>End Date: {end_date}</span></p>
						<p>Available Seats: {seats} <br /><span>Enrollees: {count}</span></p>
					</Card.Text>
					{isOpen ?
					<Button className="btn btn-block" variant="success" onClick={enroll}>
						Enroll
					</Button>
					:
					<Button className="btn btn-block" variant="secondary" disabled>
						Unavailable
					</Button>
					}
				</Card.Body>
			</Card>
		</Container>
	);
};

// PropTypes
CourseCard.propTypes = {
	data: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired,
		start_date: PropTypes.string.isRequired,
		end_date: PropTypes.string.isRequired
	})
};