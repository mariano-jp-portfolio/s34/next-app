// Base import
import { Fragment, useContext } from 'react';

// Link
import Link from 'next/link';

// React-Bootstrap
import { Navbar, Nav } from 'react-bootstrap';

// UserContext import
import UserContext from '../UserContext';

// Export Navbar functional component
export default function NavBar() {
	// consume the UserContext and destructure it
	const { user } = useContext(UserContext);
	return (
		<Navbar expand="lg" bg="dark" variant="dark">
			<Link className="navbar-brand" href="/">
				<a role="button" id="brand">Course Booking</a>
			</Link>
			<Navbar.Toggle aria-controls="basic-navbar-nav" />
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="mr-auto">
					<Link href="/courses">
						<a className="nav-link" role="button">Courses</a>
					</Link>
					{(user.email !== null)
						?
						(user.isAdmin === true)
							?
							<Fragment>
								<Link href="#">
									<a className="nav-link" role="button">Add Course</a>
								</Link>
								<Link href="/logout">
									<a className="nav-link" role="button">Log Out</a>
								</Link>
							</Fragment>
							:
							<Link href="/logout">
								<a className="nav-link" role="button">Log Out</a>
							</Link>
						:
						<Fragment>
							<Link href="/login">
								<a className="nav-link" role="button">Log In</a>
							</Link>
							<Link href="#">
								<a className="nav-link" role="button">Register</a>
							</Link>
						</Fragment>
					}
				</Nav>
			</Navbar.Collapse>
		</Navbar>
	);
};